<?php

	define('ERROR__BAD_PARAMS', -1);
	define('ERROR__ACTION_ENDED', -2);
	define('ERROR__UNKNOWN', -666);

	function processor_processRequest() {

		if( !validator_doesActionLast() ) {
			return ERROR__ACTION_ENDED;
		}

		global $get_pbid, $get_email, $get_memid, $get_month, $get_nParams;

		$validationResults = validator_areParamsValid($get_pbid, $get_email, $get_memid, $get_month, $get_nParams);
		
		if( $validationResults['nparams'] && $validationResults['pbid'] && $validationResults['memid'] && $validationResults['email'] && $validationResults['month'] ) {
			$paramsInBase = validator_areParamsInBase($get_pbid, $get_memid, $get_email);
 			if( !$paramsInBase['email'] && !$paramsInBase['memid'] && !$paramsInBase['pbid']) {
 				db_insertMemberRecord($get_email, $get_memid, $get_pbid, $get_month);
 				$returnData['result'] = 'OK';
 				$returnData['status'] = 'CREATED';
				return $returnData;
			} else if( db_isMemberIdPairedWithEmail($get_memid, $get_email) ) {
				db_updateMemberRecordMonth($get_pbid, $get_month);
				$returnData['result'] = 'OK';
				$returnData['status'] = 'UPDATED';
				return $returnData;
			} else if($paramsInBase['memid']) {
				$returnData['result'] = 'ERROR';
				$returnData['status'] = 'MEMID_EXISTS';
				return $returnData;
			} else if($paramsInBase['email']) {
				$returnData['result'] = 'ERROR';
				$returnData['status'] = 'EMAIL_EXISTS';
				return $returnData;
			} else {
				return ERROR__UNKNOWN;
			}
		} else {
			return ERROR__BAD_PARAMS;
		}
	}


/*//////////////////////////*
 * FUNKCJE ŁADUJĄCE WIDOKI: *
 *//////////////////////////*/

	function processor_loadView($requestResults) {
		
		global $get_pbid, $get_month;
		/*
		 * array(3) 
		 *     {
		 *     		["result"]=> string(2) "OK/ERROR"
		 *     		["status"]=> string(7) "CREATED/UPDATED/ERROR_TYPE"
		 *     }
		 */
		if( !is_array($requestResults) && ($requestResults == ERROR__ACTION_ENDED) ) {
			include 'view_headers.php';
			include 'view_actionEnded.php';
			include 'view_footer.php';
			return;
		}

		if($requestResults['result'] == 'ERROR') {
			$view_showError = 'BŁĄD';
			processor_loadErrorView($view_showError);

		} else if ($requestResults['result'] == 'OK') {

			$view_isUpdated = false;
			$view_isCreated = false;
			$view_isCalculationMade = false;

			if($requestResults['status'] == 'CREATED') {
				$view_isCreated = true;
			} else if($requestResults['status'] == 'UPDATED') {
				$view_isUpdated = true;
			}

			if( $view_isCreated || $view_isUpdated) {

				$redirectUrl = 'http://www2.payback.pl/ekupony/52404/aktywuj/' . $get_pbid;

				$redirectBodyFile = 'view_loadRedirect.php';
				$redirectSeconds = 10;
				processor_loadRedirectView($redirectBodyFile, $redirectSeconds, $redirectUrl);
			}
		} else {
			processor_loadErrorView('BŁĄD');
		}
	}

	function processor_loadErrorView($errorText, $redirectUrl = 'http://www2.payback.pl/') {
		$view_metaRecords = array("<meta http-equiv=\"refresh\" content=\"3;url=$redirectUrl\" />");
		$view_showError = $errorText;
		include 'view_headers.php';
		if( !empty($view_showError) )
			print '<br /><br /><h1>   ' . $view_showError . '</h1>';
		include 'view_footer.php';
		
	}

	function processor_loadRedirectView($bodyFile = null, $redirectTime = 5, $redirectUrl = 'http://www2.payback.pl/') {
		$view_metaRecords = array("<meta http-equiv=\"refresh\" content=\"$redirectTime;url=$redirectUrl\" />");
		include 'view_headers.php';
		if( !empty($bodyFile) && file_exists($bodyFile) )
			include $bodyFile;
		include 'view_footer.php';
	}
