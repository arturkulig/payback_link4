<div class="thankyou">
	<p class="thankyou-note">
        Dziękujemy za wybranie miesiąca.<br />
        Za chwilę zostaniesz przeniesiony na stronę PAYBACK, gdzie zostanie aktywowany
        Twój eKupon z punktami promocyjnymi za zakup ubezpieczenia OC/AC w LINK4.
	</p>
	<img src="images/car_purple.png" class="thankyou-car" />
	<p  class="thankyou-spinner">
	    <img src="images/loader.gif" />
	</p>
</div>
