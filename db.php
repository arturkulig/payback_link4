<?php

	include_once 'db_connector.php';

 	$backupMembersFile = 'logs/memberRecords.bak';

 	function db_isMemberIdInBase($memberId) {
 		global $db;
		$sql = 'SELECT * FROM `members` WHERE `member_id` = ?';
		$query = $db->prepare($sql);
		$query->execute( array($memberId) );
		$res = $query->fetchAll();
		return !empty($res) ?  true : false;
  	}
  	
  	function db_isPaybackIdInBase($paybackId) {
  		global $db;
  		$sql = 'SELECT * FROM `members` WHERE `payback_id` = ?';
  		$query = $db->prepare($sql);
  		$query->execute( array($paybackId) );
  		$res = $query->fetchAll();
  		return !empty($res) ?  true : false;
  	}

  	function db_getMonthForMemberId($memberId) {
  		global $db;
  	
  		$sql = 'SELECT `month` FROM `members` WHERE `member_id` = ?';
  		$query = $db->prepare($sql);
  		$query->execute( array($memberId) );
  	
  		return $query->fetchAll();
  	}

  	function db_isMemberEmailInBase($inpEmail) {
 		global $db;
		$sql = 'SELECT * FROM `members` WHERE `member_email` = ?';
		$query = $db->prepare($sql);
		$query->execute( array($inpEmail) );
		$res = $query->fetchAll();
		return !empty($res) ?  true : false;
  	}

  	function db_getAllData() {
  		global $db;
  		$sql = 'SELECT * FROM `members`';
  		$query = $db->prepare($sql);
  		$query->execute();
  		$res = $query->fetchAll();
  		return $res;
  	}
 
  	function db_isMemberIdPairedWithEmail($memberId, $inpEmail) {
 		global $db;
		$sql = 'SELECT * FROM `members` WHERE `member_email` = ? AND `member_id` = ?';
		$query = $db->prepare($sql);
		$query->execute( array($inpEmail, $memberId) );
		$res = $query->fetchAll();
		return !empty($res) ?  true : false;
  	}
  	
  	function db_isPaybackIdPairedWithEmail($paybackId, $inpEmail) {
  		global $db;
  		$sql = 'SELECT * FROM `members` WHERE `member_email` = ? AND `member_id` = ?';
  		$query = $db->prepare($sql);
  		$query->execute( array($inpEmail, $memberId) );
  		$res = $query->fetchAll();
  		return !empty($res) ?  true : false;
  	}  	
	
	function db_insertMemberRecord($email, $memberId, $paybackId, $monthNumber) {
		global $db;
		$sql = 'INSERT INTO `members` (`first_click_time`, `last_click_time`, `member_id`, `payback_id`, `member_email`, `month`) VALUES ( NOW(), NOW(), ?, ?, ?, ? )';

		//BACKUP TO FILE:
		global $backupMembersFile;
		$fp = fopen($backupMembersFile, 'a');
		$backupStr = date("Y-m-d H:i:s") . ' (insert);' . $paybackId . ';' . $memberId . ';' . $email . ';' . $monthNumber . "\n";
		fwrite($fp, $backupStr);
		fclose($fp);
		/////////////////

		$query = $db->prepare($sql);
		$query->execute( array($memberId, $paybackId, $email, $monthNumber) );
	}

	function db_updateMemberRecordMonth($paybackId, $newMonth) {
		global $db;
		$sql = 'UPDATE `members` SET `month` = ?, `last_click_time` = NOW() WHERE `payback_id` = ?';

		//BACKUP TO FILE:
		global $backupMembersFile;
		$fp = fopen($backupMembersFile, 'a');
		$backupStr = date("Y-m-d H:i:s") . ' (update month);' . $paybackId . ';' . $newMonth . "\n";
		fwrite($fp, $backupStr);
		fclose($fp);
		/////////////////

		$query = $db->prepare($sql);
		$query->execute( array($newMonth, $paybackId) );
	}

