<?php
	session_start();
	ini_set('display_errors','On');
	date_default_timezone_set('Europe/Warsaw');
	
	$get_email = @$_GET['email'];
	$get_memid = @$_GET['memid'];
	$get_pbid = @$_GET['pbid'];
	$get_month = @$_GET['month'];
	$get_nParams = count($_GET);
	$get_email = str_ireplace(' ', '+', $get_email);
	
	$logFile = fopen('logs/traffic.log', 'a');
	
	if ($logFile) {
		fwrite( $logFile, '[' . date('d-m-Y H:i:s') . ' ' . $_SERVER['REMOTE_ADDR'] . ']' . "\n");
		fwrite( $logFile, "URL: " . $_SERVER['REQUEST_URI'] . "\n");
		fwrite( $logFile, "GET: " . print_r($_GET, true) );
		fwrite( $logFile, "\n\n");
		
		fclose( $logFile );
	}
	
	include_once 'db.php'; //Funkcje obsługi bazy danych.
	include_once 'validator.php'; //Funkcje sprawdzające poprawność parametrów.
	include_once 'processor.php'; //Funkcje do obsługi zapytania.
	
	$requestResults = processor_processRequest();
	processor_loadView($requestResults);
	