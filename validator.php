<?php

	define('const_getNParams', 4);

	define('DATA__START_DATE', '2015-01-25');
 	define('DATA__END_DATE', '2015-12-18'); //Data prawdziwa.

	/**
	 * Weryfikuje poprawność member id.
	 */
	function validator_isMemberIdValid($memberId) {
		if( strlen($memberId) > 1 && strlen($memberId) < 32 )
			return true;
		else
			return false;
	}

	/**
	 * @param unknown_type $inpEmail
	 * @return mixed [string email - jeśli poprawny, bool false jeśli zły]
	 */
	function validator_isEmailValid($inpEmail) {
		if(
			( filter_var($inpEmail, FILTER_VALIDATE_EMAIL) ) ||
			( strlen($inpEmail) > 4 && ( stristr($inpEmail, '@') !== FALSE) && ( stristr($inpEmail, '.') !== FALSE) )
		)
			return true;
		else
			return false;
	}

	/**
	 * Weryfikuje poprawność podanego numeru miesiąca.
	 */
	function validator_isMonthValid($inpMonth) {
		if( ctype_digit($inpMonth) && ($inpMonth >= 1) && ($inpMonth <=12) )
			return true;
		else
			return false;
	}

	/**
	 * Weryfikuje poprawność podanego numeru karty.
	 * @param $cardNumber
	 * @return boolean
	 */
	function validator_isCardNumberValid($cardNumber) {
		if ( strlen($cardNumber) != 10 ) {
			return false;
		}
		$cardNumber = '308359'.$cardNumber;
		$length = strlen($cardNumber);

		$sum = 0;

		for ($i = $length - 1; $i >= 0; $i--) {
			$digit = (int) $cardNumber[$i];
			if ( ($i % 2) == 0 ) {
				$digit *= 2;
				if ($digit > 9)
					$digit -= 9;
			}
			$sum += $digit;
		}

		if ( ($sum % 10) != 0 ) {
			return false;
		}
		return true;
	}

	/**
	 * Weryfikuje poprawność wszystkich potrzebnych parametrów i zwraca wynik w tablicy.
	 * @return boolean array
	 */
	function validator_areParamsValid($inpCardNumber, $inpEmail, $inpMemberId, $inpMonth, $inpGetNParams) {

		$results = array();

		if($inpGetNParams == const_getNParams)
			$results['nparams'] = true;
		else
			$results['nparams'] = false;

		if( validator_isMemberIdValid($inpMemberId) )
			$results['memid'] = true;
		else
			$results['memid'] = false;

		if( validator_isEmailValid($inpEmail) )
			$results['email'] = true;
		else
			$results['email'] = false;

		if( validator_isMonthValid($inpMonth) )
			$results['month'] = true;
		else
			$results['month'] = false;

		if( validator_isCardNumberValid($inpCardNumber) )
			$results['pbid'] = true;
		else
			$results['pbid'] = false;

		return $results;

	}

	function validator_doesActionLast() {
		if( (date("Y-m-d") >= DATA__START_DATE) && (date("Y-m-d") <= DATA__END_DATE) ) {
			return true;
		} else {
			return false;
		}
	}

	function validator_areParamsInBase($inpPaybackId, $inpMemberId, $inpEmail) {

		$results = array();

		if( db_isMemberEmailInBase($inpEmail) )
			$results['email'] = true;
		else
			$results['email'] = false;

		if( db_isMemberIdInBase($inpMemberId) )
			$results['memid'] = true;
		else
			$results['memid'] = false;

		if( db_isPaybackIdInBase($inpPaybackId) )
			$results['pbid'] = true;
		else
			$results['pbid'] = false;

		return $results;

	}
